<?php
function tentukan_nilai($number)
{
    $index = '';
    if ($number >= 85 && $number < 100) {
        $index = "Sangat Baik";
    } elseif ($number >= 70 && $number < 85) {
        $index = "Baik";
    } elseif ($number >= 60 && $number < 70) {
        $index = "Cukup";
    } else {
        $index = "Kurang";
    }
    return $index . "<br>";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>